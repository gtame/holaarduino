/*
 * main.cpp
 *
 *  Created on: 17/10/2015
 *  Author: Gabriel Tamé
 */
//Referencia a la libreria comun de avr
#include <string.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/wdt.h>
//Referencia al core de arduino
#include <Arduino.h>
//Referencia a la libreria Software serial
#include <SoftwareSerial.h>

//NO BORRAR - esta funcion puede ser llamada durante la construccion/destruccion de objetos
extern "C" void __cxa_pure_virtual() {}



SoftwareSerial mySerial(10, 11); // RX, TX


//Configuramos pins, serials y lo que se ejecuta en la inicialización
void setup() {

	//Configura puerto Serie
	Serial.begin(9600);

	//Pin de salida
	pinMode(13,OUTPUT);

	//Enviar msg por puerto serie
	Serial.println("Hola arduino");

	  //Configura  SoftwareSerial port
	  mySerial.begin(9600);
	  mySerial.println("Hola SoftwareSerial");
}


void loop() //----( LOOP: RUNS CONSTANTLY )----
{
	//Activa pin 13
	digitalWrite(13,HIGH);
    Serial.println("HardwareSerial->LED ON");
    mySerial.println("SoftwareSerial->LED ON");
	delay(1000);

	//Desactiva pin 13
	digitalWrite(13,LOW);
    Serial.println("HardwareSerial->LED OFF");
    mySerial.println("SoftwareSerial->LED OFF");
	delay(1000);



}

int main()
{

	//Necesario para la inicializacion de arduino - No sobreescribir
	init();

	//Setup - Para guardar 'compatibilidad' con arduino
	setup();


	//Bucle infinito Loop - Para guardar 'compatibilidad' con arduino
	while (true)
		loop();

}


